import enmapboxtestdata
from hubdc.core import *

raster = openRasterDataset(filename=enmapboxtestdata.enmap)
array = raster.array()
result = RasterDataset.fromArray(array=array, grid=raster.grid(),
                                 filename='raster.bsq', driver=EnviDriver())
