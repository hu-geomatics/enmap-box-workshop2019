import enmapboxtestdata
from hubdc.core import *

raster = openRasterDataset(filename=enmapboxtestdata.enmap)
result = MemDriver().create(grid=raster.grid(), bands=raster.zsize(), gdalType=raster.gdalType())

for subgrid, i, iy, ix in raster.grid().subgrids(size=256):

    array = raster.array(grid=subgrid)
    result.writeArray(array=array, grid=subgrid)
