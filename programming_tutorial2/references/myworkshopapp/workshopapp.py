# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

import os
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QMenu, QAction, QWidget, QHBoxLayout, QLabel, QPushButton
from enmapbox.gui.applications import EnMAPBox, EnMAPBoxApplication
from qgis.core import *


APP_DIR = os.path.dirname(__file__)

class MyEnMAPBoxWorkshopApplication(EnMAPBoxApplication):
    """
    This Class inherits from an EnMAPBoxApplication
    """
    def __init__(self, enmapBox, parent=None):
        super(MyEnMAPBoxWorkshopApplication, self).__init__(enmapBox, parent=parent)

        #specify the name of this app
        self.name = 'My EnMAPBoxWorkshop App'
        self.version = '0.1'

        #specify a licence under which you distribute this application
        self.licence = 'GNU GPL-3'

    def icon(self)->QIcon:
        """
        This function returns the QIcon of your Application
        :return: QIcon()
        """
        return QIcon(os.path.join(APP_DIR, 'icon.svg'))

    def menu(self, appMenu)->QMenu:
        """
        Returns a QMenu that will be added to the parent `appMenu`
        :param appMenu:
        :return: QMenu
        """
        assert isinstance(appMenu, QMenu)
        """
        Specify menu, submenus and actions that become accessible from the EnMAP-Box GUI
        :return: the QMenu or QAction to be added to the "Applications" menu.
        """

        # this way you can add your QMenu/QAction to an other menu entry, e.g. 'Tools'
        # appMenu = self.enmapbox.menu('Tools')

        menu = appMenu.addMenu('My Workshop app')
        menu.setIcon(self.icon())

        # add a QAction that starts a process of your application.
        # In this case it will open your GUI.
        a = menu.addAction('Start App')
        assert isinstance(a, QAction)
        a.triggered.connect(self.startGUI)
        appMenu.addMenu(menu)

        return menu

    def startGUI(self, *args):
        """
        Opens a GUI
        :param args:
        :return:
        """
        from myworkshopapp.enmapboxworkshopui import EnMAPBoxWorkshopUI
        w = EnMAPBoxWorkshopUI()
        w.show()
        # keep a reference to the widget. If not, the garbage collector will remove it
        self.w = w

        # request & receive SpectralProfiles from EnMAP-Box
        if isinstance(self.enmapbox, EnMAPBox):
            self.enmapbox.sigCurrentSpectraChanged.connect(w.addSpectralProfiles)
            w.sigSpectralProfileRequest.connect(self.activateSpectralProfileCollection)

    def activateSpectralProfileCollection(self):
        """
        Activates the EnMAP-Box Identify location mode + identify profiles
        """
        if isinstance(self.enmapbox, EnMAPBox):
            self.enmapbox.ui.actionIdentify.setChecked(True)
            self.enmapbox.ui.optionIdentifyProfile.setChecked(True)