from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *

from enmapbox.testing import initQgisApplication

app = initQgisApplication()
from enmapbox.gui import TreeView, TreeModel, TreeNode

treeModel = TreeModel()
treeView = TreeView()
treeView.setModel(treeModel)
treeView.setWindowTitle('enmapbox.gui.TreeView')
treeView.show()

n1 = TreeNode(treeModel.rootNode(),
              name='First Tree Node',
              icon=QIcon(':/enmapbox/gui/ui/icons/enmapbox.svg'))
n1.setValue(42)
n1.setIcon(QIcon(':/enmapbox/gui/ui/icons/enmapbox.svg'))

n2 = TreeNode(treeModel.rootNode(), name='Node 2')
n2.setName('Second Tree Node, without value')
n1.setIcon(QIcon(':/enmapbox/gui/ui/icons/enmapbox.svg'))

# define sub nodes
sub1 = TreeNode(n1, name='Sub node 1.1')
sub2 = TreeNode(None, name='Sub node 1.2')
n1.appendChildNodes([sub2])

sub3 = TreeNode(n1, name='Sub node 2.1')
n2.appendChildNodes(sub3)

qtTreeView = QTreeView()
qtTreeView.setWindowTitle('PyQt5.QtWidgets.QTreeView')
qtTreeView.setModel(treeModel)
qtTreeView.show()

qtTableView = QTableView()
qtTableView.setWindowTitle('PyQt5.QtWidgets.QTableView')
qtTableView.setModel(treeModel)
qtTableView.show()
app.exec_()
