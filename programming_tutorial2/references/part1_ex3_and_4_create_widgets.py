from qgis.core import *
from qgis.gui import *
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *


class ExampleWidget(QWidget):

    def __init__(self, parent=None):
        super(ExampleWidget, self).__init__(parent)

        self.setWindowTitle('Example Widget')
        self.resize(QSize(300, 200))

        self.textBox = QTextEdit()
        self.mapCanvas = QgsMapCanvas()
        self.label = QLabel('Label info')
        self.button = QPushButton('Press me')

        self.setLayout(QVBoxLayout())

        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(self.textBox)
        self.topLayout.addWidget(self.mapCanvas)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addWidget(self.label)
        self.bottomLayout.addWidget(self.button)

        self.layout().addLayout(self.topLayout)
        self.layout().addLayout(self.bottomLayout)

        from enmapboxtestdata import enmap
        layer = QgsRasterLayer(enmap)
        QgsProject.instance().addMapLayer(layer)
        self.mapCanvas.setLayers([layer])
        self.mapCanvas.setDestinationCrs(layer.crs())
        self.mapCanvas.setExtent(self.mapCanvas.fullExtent())

        # connect the signal 'clicked' with slot 'onButtonClicked'
        self.button.clicked.connect(self.onButtonClicked)

        # connect the signal 'clicked' with a lambda function
        self.button.clicked.connect(
            lambda: self.mapCanvas.setExtent(
                self.mapCanvas.fullExtent()))

        # emit own signals
        self.button.clicked.connect(lambda: self.sigButtonClicked.emit())
        self.button.clicked.connect(lambda: self.sigButtonClicked[str].emit('Button  clicked'))
        print('Button clicked')
    def onButtonClicked(self, *args):
        text = self.textBox.toPlainText()
        text = text + '\nclicked'
        self.textBox.setText(text)

        from datetime import datetime
        self.label.setText('Last click {}'.format(datetime.now().time()))

    sigButtonClicked = pyqtSignal([], [str])



from enmapbox.testing import initQgisApplication
app = initQgisApplication()

myWidget = ExampleWidget()
myWidget.show()

# specify signals
myWidget.sigButtonClicked[str].connect(lambda text : print('Received: {}'.format(text)))

app.exec_()