
# see programming_tutorial2/references/myworkshopapp/tests.py

from enmapbox.testing import initQgisApplication
APP = initQgisApplication()

from enmapbox import EnMAPBox, EnMAPBoxApplication
from myworkshopapp.workshopapp import MyEnMAPBoxWorkshopApplication

enmapBox = EnMAPBox(None)
enmapBox.loadExampleData()

myApp = MyEnMAPBoxWorkshopApplication(enmapBox)
assert isinstance(myApp, EnMAPBoxApplication)
enmapBox.addApplication(myApp)

APP.exec_()