from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *


class ContextMenuExample(QWidget):

    def __init__(self, *args):
        super(ContextMenuExample, self).__init__(*args)
        self.resize(QSize(450, 100))
        self.setWindowTitle('ContextMenuExample')

        self.option1 = True
        self.option2 = False

    def contextMenuEvent(self, event: QContextMenuEvent):
        menu = QMenu()

        action1 = menu.addAction('Print "A"')
        action1.triggered.connect(lambda: print('A'))

        action2 = menu.addAction('Print "B"')
        action2.triggered.connect(lambda: print('B'))

        menu.addSeparator()
        subMenu = menu.addMenu('More actions..')
        action3 = subMenu.addAction('Print "C.1"')
        action3.triggered.connect(lambda: print('C.1'))

        action4 = subMenu.addAction('Print "C.2"')
        action4.triggered.connect(lambda: print('C.2'))

        subMenu = menu.addMenu('Options...')

        opt1 = subMenu.addAction('Option 1')
        opt1.setCheckable(True)
        opt1.setChecked(self.option1 == True)
        opt1.toggled.connect(lambda b: setattr(self, "option1", b))

        opt2 = subMenu.addAction('Option 2')
        opt2.setCheckable(True)
        opt2.setChecked(self.option2 == True)
        opt2.toggled.connect(lambda b: setattr(self, "option2", b))

        menu.exec_(event.globalPos())


app = QApplication([])
w = ContextMenuExample()
w.show()
app.exec_()